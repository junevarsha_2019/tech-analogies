import { createContext } from "react";

const ThemeContext = createContext({name: '', id: '', userLoggedIn: ''});

export default ThemeContext;
